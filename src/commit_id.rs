#[derive(PartialEq,Eq)]
pub enum CommitId {
	Sha1([u8; 20]),
	Sha256([u8; 32]),
}

impl CommitId {
	fn as_u8(&self) -> &[u8] {
		match self {
			CommitId::Sha1(b) => b,
			CommitId::Sha256(b) => b,
		}
	}
}

impl std::fmt::Display for CommitId {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		for b in self.as_u8() {
			write!(f, "{:02x}", b)?;
		}
		Ok(())
	}
}

impl std::fmt::Debug for CommitId {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		write!(f, "{}", self)
	}
}

impl std::str::FromStr for CommitId {
	type Err = anyhow::Error;

	fn from_str(s: &str) -> Result<Self, Self::Err> {
		match s.len() {
			40 => Ok(CommitId::Sha1(
				crate::hex_to_buf(s.as_bytes())
					.ok_or_else(||
						anyhow::Error::msg(format!("Commit ID has invalid character {:?}", s)))?)),
			64 => Ok(CommitId::Sha256(
				crate::hex_to_buf(s.as_bytes())
					.ok_or_else(||
						anyhow::Error::msg(format!("Commit ID has invalid character {:?}", s)))?)),
			_ => Err(anyhow::Error::msg(format!("Commit ID has invalid length {:?}", s))),
		}
	}
}

#[test]
fn test_commit_id_parse() {
	let sha1_str = "c2d83a371de4cae5a317df2735ef25f459e5ca47";
	let sha1_bin = sha1_str.parse::<CommitId>().unwrap();
	assert_eq!(
		sha1_bin,
		CommitId::Sha1([0xc2, 0xd8, 0x3a, 0x37, 0x1d, 0xe4, 0xca, 0xe5, 0xa3, 0x17, 0xdf, 0x27, 0x35, 0xef, 0x25, 0xf4, 0x59, 0xe5, 0xca, 0x47]));
	assert_eq!(
		sha1_bin.to_string(),
		sha1_str);

	let sha256_str = "f7971a8cbdbad9fd03d54e7fd00e5875cfa81b039073ba70022a1d44544849c3";
	let sha256_bin = sha256_str.parse::<CommitId>().unwrap();
	assert_eq!(
		sha256_bin,
		CommitId::Sha256([0xf7, 0x97, 0x1a, 0x8c, 0xbd, 0xba, 0xd9, 0xfd, 0x03, 0xd5, 0x4e, 0x7f, 0xd0, 0x0e, 0x58, 0x75, 0xcf, 0xa8, 0x1b, 0x03, 0x90, 0x73, 0xba, 0x70, 0x02, 0x2a, 0x1d, 0x44, 0x54, 0x48, 0x49, 0xc3]));
	assert_eq!(
		sha256_bin.to_string(),
		sha256_str);
}

