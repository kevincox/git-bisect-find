pub fn hex_to_nibble(c: u8) -> Option<u8> {
	Some(match c {
		b'0'..=b'9' => c - b'0',
		b'A'..=b'Z' => 10 + c - b'A',
		b'a'..=b'z' => 10 + c - b'a',
		_ => return None,
	})
}

pub fn hex_to_buf<const N: usize>(s: &[u8]) -> Option<[u8; N]> {
	let mut buf = [0; N];

	debug_assert_eq!(s.len(), N * 2);
	if s.len() != N * 2 {
		return None
	}

	for i in 0..(s.len()/2) {
		buf[i] = (hex_to_nibble(s[i*2])? << 4) + hex_to_nibble(s[i*2 + 1])?;
	}

	Some(buf)
}
