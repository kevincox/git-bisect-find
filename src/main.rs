mod commit_id; pub use commit_id::*;
mod git_bisect_log; pub use git_bisect_log::*;
mod git_bisect_next; pub use git_bisect_next::*;
mod hex; pub use hex::*;
mod std_utils; pub use std_utils::*;

use std::os::unix::process::CommandExt;

#[derive(Debug,clap::Subcommand)]
pub enum Command {
	/** Mark the current commit as bad, find new candidate.

		`git bisect-find bad` is analogous to `git bisect bad`. It does two main things:

		- Mark the current commit as bad.
		- Find a new candidate commit.

		The new candidate is picked by exponential growth from the last bad commit. This ensures that the search will quickly find an anchor point.
	*/
	Bad,

	/** Mark the current commit as good.

		This is just an alias for `git bisect good` with a help message.

		Once you have found a good commit `git bisect-find`'s job is complete. You should switch over to regular `git bisect` commands. Running this command is never necessary and just provides a reminder for your muscle memory or mistyped commands.
	*/
	Good,

	/** Skip the current commit, find a new candidate.

		`git bisect-find bad` is analogous to `git bisect bad`. It does two main things:

		- Mark the current commit as untestable.
		- Find a new candidate commit.
	*/
	Skip,
}

/** Find range for a bisection.

	When you have a "bad" commit but don't yet know a "good" commit `git bisect-find` will help you. The basic flow looks like:

	1. Checkout the known bad commit.

	2. Run `git bisect-find bad`.

	3. Test the new commit, if it is still bad repeat from step 2.

	4. If the commit can't be tested, run `git bisect-find skip` and repeat from step 3.

	5. Once a good commit is found switch to `git bisect`, starting with `git bisect good`.

*/
#[derive(Debug,clap::Parser)]
#[clap(version)]
pub struct Args {
	/** Foo bar bas */
	#[clap(subcommand)]
	pub command: Option<Command>,
}

fn main() -> anyhow::Result<()> {
	let Args { command }: Args = clap::Parser::parse();

	match command.unwrap_or(Command::Bad) {
		Command::Bad => {
			wait_ok(
				std::process::Command::new("git")
					.args(&["bisect", "bad"])
					.spawn()?,
				"git bisect bad")?;
			git_bisect_next()?;
		}
		Command::Good => {
			eprintln!("Good commit found, git bisect-find is done.");
			std::process::Command::new("git")
				.args(&["bisect", "good"])
				.exec();
		}
		Command::Skip => {
			wait_ok(
				std::process::Command::new("git")
					.args(&["bisect", "skip"])
					.spawn()?,
				"git bisect skip")?;
			git_bisect_next()?;
		}
	}

	Ok(())
}
