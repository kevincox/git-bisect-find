#[derive(Debug,PartialEq,Eq)]
pub struct BisectLog {
	pub base: crate::CommitId,
	pub bad_count: u32,
	pub skip_count: u32,
}

pub enum BisectMarkType {
	Bad,
	Good,
	Skip,
}

impl std::fmt::Display for BisectMarkType {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		f.write_str(match self {
			BisectMarkType::Bad => "bad",
			BisectMarkType::Good => "good",
			BisectMarkType::Skip => "skip",
		})
	}
}

pub struct BisectMark {
	mark: BisectMarkType,
	commit: crate::CommitId,
	// message: String,
}

pub fn parse_mark(mark: BisectMarkType, rest: &str) -> anyhow::Result<BisectMark> {
	let Some((commit, _message)) = rest.strip_prefix('[')
		.and_then(|rest| rest.split_once("] "))
		else {
			anyhow::bail!("Expected `[{{commit}}]` at start of {} action", mark)
		};

	Ok(BisectMark {
		mark,
		commit: commit.parse()?,
	})
}

pub enum BisectAction {
	Mark(BisectMark),
	Status,
}

impl std::str::FromStr for BisectAction {
	type Err = anyhow::Error;

	fn from_str(s: &str) -> Result<Self, Self::Err> {
		let Some((action, rest)) = s.split_once(": ") else {
			anyhow::bail!("Expected `{{action}}: `")
		};

		Ok(match action {
			"bad" => BisectAction::Mark(parse_mark(BisectMarkType::Bad, rest)?),
			"good" => BisectAction::Mark(parse_mark(BisectMarkType::Good, rest)?),
			"skip" => BisectAction::Mark(parse_mark(BisectMarkType::Skip, rest)?),
			"status" => BisectAction::Status,
			_ => anyhow::bail!("Unknown bisection action {:?}", s),
		})
	}
}

pub fn parse_git_bisect(log: impl std::io::BufRead) -> anyhow::Result<BisectLog> {
	let mut base = None;
	let mut bad_count = 0;
	let mut skip_count = 0;

	for line in log.lines() {
		let line = line?;
		
		let Some(meta_line) = line.strip_prefix("# ") else { continue };
		let action = meta_line.parse::<BisectAction>()
			.map_err(|e| e.context(format!("Parsing {:?}", meta_line)))?;

		match action {
			BisectAction::Mark(mark) => {
				match mark.mark {
					BisectMarkType::Bad => {
						bad_count += 1;
						base = Some(mark.commit);
					}
					BisectMarkType::Good => {
						eprintln!("Warning, good commit in log. What are you looking for?");
					}
					BisectMarkType::Skip => {
						skip_count += 1;
					}
				}
			}
			BisectAction::Status => {}
		}
	}

	Ok(BisectLog {
		base: base.ok_or_else(|| anyhow::Error::msg("No commits in log"))?,
		bad_count,
		skip_count,
	})
}

#[test]
fn test_parse_git_bisect() {
	let log = r###"
# bad: [8dbe7aab8627da76d5dff8930c6da2b04aa2ee0f] Message 1
# good: [40de07749cc9182d6bfe35d418937b73f6117a9b] [BUG] Message 2
git bisect start 'HEAD' 'HEAD~200'
# bad: [ba1163d41817eeecc57d32c9bb4e9c3194bc0419] ]: Weid Message
git bisect bad ba1163d41817eeecc57d32c9bb4e9c3194bc0419
# good: [9a2c631a9bf311e7af08db2a5f74e16d59e791a5] Message 3
git bisect good 9a2c631a9bf311e7af08db2a5f74e16d59e791a5
# bad: [f7971a8cbdbad9fd03d54e7fd00e5875cfa81b03] Message 4
git bisect bad f7971a8cbdbad9fd03d54e7fd00e5875cfa81b03
"###;
	assert_eq!(
		parse_git_bisect(std::io::Cursor::new(log)).unwrap(),
		BisectLog {
			base: "f7971a8cbdbad9fd03d54e7fd00e5875cfa81b03".parse().unwrap(),
			bad_count: 3,
			skip_count: 0,
		});

	let log = r###"
# bad: [8dbe7aab8627da76d5dff8930c6da2b04aa2ee0f] Message 1
git bisect start 'HEAD'
# bad: [ba1163d41817eeecc57d32c9bb4e9c3194bc0419] ]: Weid Message
git bisect bad ba1163d41817eeecc57d32c9bb4e9c3194bc0419
# skip: [36eef9de0c90fe5b895f1415bbc02600e08972c7] Message 2
git bisect skip 36eef9de0c90fe5b895f1415bbc02600e08972c7
# bad: [f7971a8cbdbad9fd03d54e7fd00e5875cfa81b039073ba70022a1d44544849c3] Message 4
git bisect bad f7971a8cbdbad9fd03d54e7fd00e5875cfa81b039073ba70022a1d44544849c3
# skip: [713bb5478843c2c2c5386f1d4873454211189db2] Doesn't build
git bisect skip 713bb5478843c2c2c5386f1d4873454211189db2
"###;
	assert_eq!(
		parse_git_bisect(std::io::Cursor::new(log)).unwrap(),
		BisectLog {
			base: "f7971a8cbdbad9fd03d54e7fd00e5875cfa81b039073ba70022a1d44544849c3".parse().unwrap(),
			bad_count: 3,
			skip_count: 2,
		});
}

pub fn git_bisect_log() -> anyhow::Result<BisectLog> {
	let mut proc = std::process::Command::new("git")
		.args(&["bisect", "log"])
		.stdin(std::process::Stdio::null())
		.stdout(std::process::Stdio::piped())
		.spawn()?;

	let stdout = std::io::BufReader::new(proc.stdout.take().unwrap());

	let log = parse_git_bisect(stdout)?;

	crate::wait_ok(proc, "git bisect log")?;

	Ok(log)
}

