pub fn wait_ok(
	mut cmd: std::process::Child,
	desc: &'static str,
) -> anyhow::Result<()> {
	let status = cmd.wait()?;
	if status.success() {
		Ok(())
	} else {
		Err(anyhow::Error::msg(format!("{} exited {}", desc, status.code().unwrap_or(-1))))
	}
}
