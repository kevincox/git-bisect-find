pub fn git_bisect_next() -> anyhow::Result<()> {
	let log = crate::git_bisect_log()?;

	let jump = 2u64.saturating_pow(4 + log.bad_count + log.skip_count);

	let checkout = crate::wait_ok(
		std::process::Command::new("git")
			.arg("checkout")
			.arg(format!("{}~{}", log.base, jump))
			.stdin(std::process::Stdio::null())
			.spawn()?,
		"git checkout");

	let err = match checkout {
		Ok(()) => return Ok(()),
		Err(e) => e,
	};

	eprintln!("{} assuming reached start, checking out first commit", err);

	let mut orphan_commits = std::process::Command::new("git")
		.args(&[
			"log",
			&log.base.to_string(),
			"--max-parents=0",
			"--format=format:%H",
		])
		.stdin(std::process::Stdio::null())
		.stdout(std::process::Stdio::piped())
		.spawn()?;

	let stdout = std::io::BufReader::new(orphan_commits.stdout.take().unwrap());
	let mut first_commit = None;
	for line in std::io::BufRead::lines(stdout) {
		// I'm not sure if there is a smarter way to determine the best initial commit. But 99% of the time there will only be one commit with no parents. In other cases we just pick the last one as a heuristic.
		first_commit = Some(line?);
	}

	let Some(first_commit) = first_commit else {
		anyhow::bail!("Could not find initial commit.")
	};

	if first_commit.parse::<crate::CommitId>()? == log.base {
		anyhow::bail!("All commits marked bad.")
	}

	crate::wait_ok(orphan_commits, "git log --max-parents=0")?;

	crate::wait_ok(
		std::process::Command::new("git")
			.args([
				"checkout",
				&first_commit,
			])
			.stdin(std::process::Stdio::null())
			.spawn()?,
		"git checkout")
}
