{
	nixpkgs ? import <nixpkgs> {},
	naersk ? nixpkgs.pkgs.callPackage (fetchTarball "https://github.com/nix-community/naersk/archive/master.tar.gz") {},
}: let
	inherit (nixpkgs) lib;
in {
	git-bisect-find = naersk.buildPackage {
		root = lib.fileset.toSource {
			root = ./.;
			fileset = lib.fileset.unions [
				./Cargo.lock
				./Cargo.toml
				./src
			];
		};
	};
}
