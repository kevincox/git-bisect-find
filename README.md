# A simple command to find where to start your bisection.

[`git bisect`](https://git-scm.com/docs/git-bisect) is a great command. Given a "bad" commit (commit with some change) and a "good" commit (a commit before that change) it can tell you where the change was introduced.

However the problem is that often you identify a problem, but don't have an anchor point before that problem existed. This is where `git bisect-find` steps in. It searches back through your history until a "good" commit is found, then launches `git bisect` to find the exact originator.

## Usage

Basic usage looks like this:

### Starting

First checkout a "bad" commit. Often this is just your main development branch.

```sh
$ git checkout main
```

Then start bisecting with `git bisect-find bad`.

```sh
$ git bisect-find bad
You need to start by "git bisect start"

Do you want me to do it for you [Y/n]? y
status: waiting for both good and bad commits
status: waiting for good commit(s), bad commit known
Previous HEAD position was c2d83a37 Remove outdated flag.
HEAD is now at 2016f0d Set version to zero.
```

You will be asked to start a regular `git bisect` run, type <kbd>y</kbd>.

`git bisect-find` will then check out a new commit to test. This works just like regular `git bisect`! The difference is that instead of narrowing the search range `git bisect-find` will be checking out older and older commits in search of a "good" anchor point.

### Marking Bad Commits

While the commit is still bad run `git bisect-find bad`. This will record the current commit as bad and check out a new candidate.

```sh
% git bisect-find bad
status: waiting for good commit(s), bad commit known
Previous HEAD position was 2016f0d Set version to zero.
HEAD is now at ed4a4f6 Clean up WeirdTrie.
```

### Skipping Commits

If the current commit can't be tested for any reason (maybe it fails to build) just run `git bisect-find skip`. Much like `git bisect skip` this will find a new candidate commit to test.

### Finding a Good Commit

Once you find a good commit you are done with `git bisect-find`. Your `git` is already in a regular bisection, so you can switch over to the native `git bisect` commands. Just run `git bisect good` and continue from there.

All of your `bad` and `skip` commits are already known to Git, so you don't need to transfer any state and you will only have to bisect between the latest `bad` commit and the `good` commit you just found.
